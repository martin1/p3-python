import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_frecuency(self):
        sin_sound = SoundSin(1, 440, 1)
        self.assertEqual(sin_sound.frequency, 440)

    def test_amplitude(self):
        sin_sound = SoundSin(1, 440, 2)
        self.assertEqual(sin_sound.amplitude, 2)

    def test_duration(self):
        sin_sound = SoundSin(5, 440, 1)
        self.assertEqual(sin_sound.duration, 5)


if __name__ == '__main__':
    unittest.main()
