import unittest
from mysound import Sound
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):

    def test_same_length(self):
        s1 = Sound(1)
        s2 = Sound(1)
        expected = 1
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, expected)

    def test_different_lengths(self):
        s1 = Sound(1)
        s2 = Sound(4)
        expected = 4
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, expected)

    def test_empty_sound(self):
        s1 = Sound(0)
        s2 = Sound(3)
        expected = 3
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, expected)


if __name__ == '__main__':
    unittest.main()
